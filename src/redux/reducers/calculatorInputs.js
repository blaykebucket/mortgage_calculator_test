let initState = {
  propPrice: 450000,
  propLoanAmt: 450000 * 0.75,
  propTenureYears: 25
};

const calculatorInputs = (state = initState, action) => {
  let validValue = null;

  switch (action.type) {
    case "changePropValue":
      validValue = parseInt(action.payload);
      return Object.assign({}, state, {
        propPrice: validValue ? validValue : 0,
        propLoanAmt: Math.ceil(validValue * 0.75) || ""
      });
    case "changeLoanAmt":
      validValue = parseInt(action.payload);
      return Object.assign({}, state, {
        propLoanAmt: validValue ? validValue : 0
      });
    case "changeTenure":
      return Object.assign({}, state, {
        propTenureYears: parseInt(action.payload)
      });
    case "resetValues":
      return Object.assign({}, state, initState);
    default:
      return state;
  }
};

module.exports = calculatorInputs;
