let formData = {
  formIsClosed: true,
  formNumChosen: null,
  formSubmitted: false,
  formHasError: null,
  fullname: { value: "", valid: false },
  email: { value: "", valid: false },
  mobile: { value: "", valid: false }
};

const formCallBack = (state = formData, action) => {
  switch (action.type) {
    case "registerFields":
      return Object.assign({}, state, { ...action.payload });
    case "toggleForm":
      return Object.assign({}, state, {
        formIsClosed: !state.formIsClosed,
        formNumChosen: parseInt(action.payload),
        formSubmitted: false,
        formHasError: null
      });
    case "formSubmitted":
      return Object.assign({}, state, {
        formSubmitted: true,
        formHasError: null
      });
    case "formHasError":
      return Object.assign({}, state, {
        formHasError: action.payload,
        formSubmitted: false
      });
    default:
      return state;
  }
};

export default formCallBack;
