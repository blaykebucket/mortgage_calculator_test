const bankSingleInfo = {
  bankInfo: null,
  selectedBank: null
}



const singleBankInfo = (state = bankSingleInfo, action) => {
  switch (action.type) {
    case "setBankInfo":
      const data = action.payload.results[0],
        id = action.payload.results[0]._id;
      return Object.assign({}, state, { bankInfo: data, selectedBank: id })
    case "showInfo":
      return state;
    default:
      return state;
  }
}


export default singleBankInfo;