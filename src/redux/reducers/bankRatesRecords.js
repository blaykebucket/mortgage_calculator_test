let initState = {
  topBanks: []
}


const bankRatesRecords = (state = initState, action) => {
  switch (action.type) {
    case "registerBankRates":
      //map the bank records here and try to filter the best rate of all
      const topBanks = action.payload.map((each) => {
        return {
          bankId: each._id,
          bankName: each.bankName,
          bankImage: each.bankImageUrl,
          bankRateName: each.rateName,
          bankRate: parseFloat(each.interestRates[0]),
          allRates: each.interestRates,
        }
      }).sort((a, b) => {
        return a.bankRate - b.bankRate
      }).slice(0, 5).sort((a,b)=>{
              const aBank =  a.allRates.reduce((accum, next)=>{
                  return accum  + parseFloat(next)
              },0)
              const bBank =  b.allRates.reduce((accum, next)=>{
                  return accum  + parseFloat(next)
              },0)
              return aBank - bBank;            
      });
      return Object.assign({}, state, { topBanks })
    default:
      return state;
  }
}


module.exports = bankRatesRecords;