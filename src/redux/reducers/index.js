import { combineReducers } from 'redux'
import calcInputs from './calculatorInputs'
import bankRates from './bankRatesRecords'
import SingleBank from './singleBankInfo'
import formCallBack from './formCallback'

export const allReducer = combineReducers({
  calcInputs,
  bankRates,
  SingleBank,
  formCallBack
})