export const registerFields = val => {
  return {
    type: "registerFields",
    payload: val
  };
};

export const toggleForm = val => {
  return {
    type: "toggleForm",
    payload: val
  };
};

export const formSubmitted = val => {
  return {
    type: "formSubmitted",
    payload: val
  };
};


export const formHasError = val => {
  return {
    type: "formHasError",
    payload: val
  };
};
