export const registerBankRates = (val) => {
  return {
    type: "registerBankRates",
    payload: val
  }
}