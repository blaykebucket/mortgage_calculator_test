export const changeTenureYears = (val) => {
  return {
    type: "changeTenure",
    payload: val
  }
}

export const changePropValue = (val) => {
  return {
    type: "changePropValue",
    payload: val
  }
}

export const changeLoanAmt = (val) => {
  return {
    type: "changeLoanAmt",
    payload: val
  }
}

export const resetValues = () => {
  return {
    type: "resetValues",
  }
}


