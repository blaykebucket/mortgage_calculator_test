import { createStore } from 'redux'
import { allReducer } from '../reducers/index'
export const mainStore = createStore(allReducer);