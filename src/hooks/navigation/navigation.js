import "./navigation.scss";
import React from 'react'
import Phone from '../widgets/phone/phone'
import Burger from '../widgets/burger/burger'
import Logo from '../widgets/logo/logo'

function Navigation() {
  return (
    <nav className="myhome-nav">
      <Phone />
      <Logo />
      <Burger />
    </nav>
  )
}

export default Navigation
