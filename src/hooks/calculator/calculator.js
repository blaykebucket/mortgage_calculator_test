import "./calculator.scss";
import React from 'react'
import CalculatorHeader from './childHooks/header'
import CalculatorFields from './childHooks/calcuFields'


function Calculator() {
  return (
    <div className="myhome-calculator">
      <CalculatorHeader />
      <CalculatorFields />
    </div>
  )
}

export default Calculator
