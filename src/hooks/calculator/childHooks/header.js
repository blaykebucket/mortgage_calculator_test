import "./header.scss";
import React from 'react'

function CalculatorHeader() {
  return (
    <div className="myhome-calculator-heading">
      <div className="myhome-heading-grp1">
        <img src="./images/calculator.svg" alt="calculator" />
      </div>
      <div className="myhome-heading-grp2">
        <h1>Monthly Payment Calculator</h1>
        <p>This calculator estimates the monthly instalment payable on a housing loan.</p>
      </div>
    </div>
  )
}

export default CalculatorHeader
