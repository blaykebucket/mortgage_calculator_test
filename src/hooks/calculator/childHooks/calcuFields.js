import "./calcuFields.scss";
import React from 'react'
import { useSelector, useDispatch } from 'react-redux'
import { changeTenureYears, changeLoanAmt, changePropValue, resetValues } from '../../../redux/actions/calculatorInputs'

function CalculatorFields() {

  const calcInputs = useSelector((state) => state.calcInputs)
  const dispatch = useDispatch();

  const filterNumeric = (val) => {
    return val.split("").filter((char) => {
      return "0123456789".indexOf(char) > -1;
    }).join("");
  }


  const covertFancyDollar = (val) => {
    return `$${val.toLocaleString()}`
  }

  const changeInputs = (e) => {
    let target = e.currentTarget.getAttribute('data-target'),
      val = e.currentTarget.value || e.currentTarget.textContent;

    switch (target) {
      case "propValue":
        dispatch(changePropValue(filterNumeric(val)))
        break;
      case "loanAmt":
        dispatch(changeLoanAmt(filterNumeric(val)))
        break;
      case "tenure":
        dispatch(changeTenureYears(val))
        break;
      case "resetValues":
        //will reset
        dispatch(resetValues())
        break;
      default:
        break;
    }
  }

  return (
    < div className="myhome-calculator-fields" >
      <div className="myhome-calculator-field-box">
        <label htmlFor="propPrice">
          Property Price:
        </label>
        <input type="text"
          name="propPrice"
          data-target="propValue"
          onChange={changeInputs}
          value={calcInputs.propPrice == "" ? covertFancyDollar(0) : covertFancyDollar(calcInputs.propPrice)}
          maxLength="11"
        />
      </div>
      <div className="myhome-calculator-field-box">
        <label htmlFor="propLoan">
          Loan Amount:
        </label>
        <input onChange={changeInputs} data-target="loanAmt" type="text" name="propLoan"
          onChange={changeInputs}
          value={calcInputs.propLoanAmt == "" ? covertFancyDollar(0) : covertFancyDollar(calcInputs.propLoanAmt)}
          maxLength="11"
        />
      </div>
      <div className="myhome-calculator-field-box">
        <label htmlFor="Tenure">
          Tenure:
        </label>
        <button data-target="tenure" onClick={changeInputs} className={calcInputs.propTenureYears == 5 ? "myhome-tenure-years selected" : "myhome-tenure-years"}>5</button>
        <button data-target="tenure" onClick={changeInputs} className={calcInputs.propTenureYears == 10 ? "myhome-tenure-years selected" : "myhome-tenure-years"}>10</button>
        <button data-target="tenure" onClick={changeInputs} className={calcInputs.propTenureYears == 15 ? "myhome-tenure-years selected" : "myhome-tenure-years"}>15</button>
        <button data-target="tenure" onClick={changeInputs} className={calcInputs.propTenureYears == 20 ? "myhome-tenure-years selected" : "myhome-tenure-years"}>20</button>
        <button data-target="tenure" onClick={changeInputs} className={calcInputs.propTenureYears == 25 ? "myhome-tenure-years selected" : "myhome-tenure-years"}>25</button>
      </div>
      <button data-target="resetValues" onClick={changeInputs} className="myhome-calculator-reset" >
        Reset Default
    </button>
    </div >
  )
}

export default CalculatorFields


