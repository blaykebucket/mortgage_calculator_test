import "./hero.scss";
import React from 'react'
import Calculator from '../calculator/calculator'


function Hero() {
  return (
    <section className="myhome-hero">
      <div className="container">
        <Calculator />
      </div>
    </section>
  )
}

export default Hero
