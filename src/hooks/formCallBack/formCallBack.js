import "./FormCallback.scss";
import React, { useEffect } from "react";
import { useSelector, useDispatch } from "react-redux";
import {
  registerFields,
  toggleForm,
  formSubmitted,
  formHasError
} from "../../redux/actions/formCallback";

function FormCallback({ bankId }) {

  const formState = useSelector(state => state.formCallBack);
  const calcInputs = useSelector(state => state.calcInputs);
  const selectedBank = useSelector(state => state.SingleBank.selectedBank);
  const dispatch = useDispatch();

  const animateText = e => {
    const parent = e.target.parentElement;
    parent.classList.add("field-animate-text");
  };
  const checkEmpty = e => {
    const parent = e.target.parentElement;
    if (!e.currentTarget.value) parent.classList.remove("field-animate-text");
  };
  const closeForm = e => {
    e.stopPropagation();
    dispatch(toggleForm("0"));
  };
  const stopPropGate = e => {
    e.stopPropagation();
  };
  const updateInputs = e => {
    const filterMobile = val => {
      return val
        .split("")
        .filter(char => {
          return "0123456789".indexOf(char) > -1;
        })
        .join("");
    };
    const checkLength = val => {
      return val.length >= 4;
    };

    const checkEmail = val => {
      return /(\S|\d)+@[a-zA-Z]{2,}.com/gi.test(val);
    };

    const target = e.currentTarget.getAttribute("name"),
      val = e.currentTarget.value;
    switch (target) {
      case "fullname":
        dispatch(
          registerFields({
            [target]: {
              value: val,
              valid: checkLength(val)
            }
          })
        );
        break;
      case "email":
        dispatch(
          registerFields({
            [target]: {
              value: val,
              valid: checkEmail(val)
            }
          })
        );
        break;
      case "mobile":
        dispatch(
          registerFields({
            [target]: {
              value: filterMobile(val),
              valid: checkLength(val)
            }
          })
        );
        break;
      default:
        break;
    }
  };
  const formCanSubmit = () => {
    const { fullname, email, mobile } = formState;
    return fullname.valid && email.valid && mobile.valid;
  };
  const formSubmit = async e => {
    const { fullname, email, mobile } = formState;
    const { propPrice, propLoanAmt, propTenureYears } = calcInputs;
    e.preventDefault();
    const uri = "http://localhost:8080/contactBank",
      request = new Request(uri, {
        method: "POST",
        mode: "cors",
        headers: new Headers({
          "Content-Type": "application/json"
        }),
        body: JSON.stringify({
          fullname: fullname.value,
          email: email.value,
          mobile: mobile.value,
          bankId: selectedBank,
          prpPrice: propPrice,
          prpLoan: propLoanAmt,
          prpTenure: propTenureYears
        })
      }),
      preData = await fetch(request),
      finalData = await preData.json();
    if (finalData.errors) {
      const errorMsg = finalData.errors[0].message;
      dispatch(formHasError(errorMsg));
    } else {
      dispatch(formSubmitted());
    }
  };

  return (
    <>
      <div
        onClick={stopPropGate}
        className={
          !formState.formIsClosed && formState.formNumChosen == bankId
            ? "myhome-callback"
            : "myhome-callback hide"
        }
      >
        <p>Please input your contact details</p>
        <form
          className="myhome-callback-form"
          autoComplete="off"
          onSubmit={formSubmit}
        >
          <label
            htmlFor="fullname"
            data-name="Fullname"
            className={
              formState.fullname.value !== "" ? "field-animate-text" : ""
            }
          >
            <input
              onFocus={animateText}
              onBlur={checkEmpty}
              type="text"
              name="fullname"
              onChange={updateInputs}
              value={formState.fullname.value}
            />
          </label>
          <label
            htmlFor="email"
            data-name="Email"
            className={formState.email.value !== "" ? "field-animate-text" : ""}
          >
            <input
              onFocus={animateText}
              onBlur={checkEmpty}
              type="text"
              name="email"
              onChange={updateInputs}
              value={formState.email.value}
            />
          </label>
          <label
            htmlFor="mobile"
            data-name="Mobile"
            className={
              formState.mobile.value !== "" ? "field-animate-text" : ""
            }
          >
            <input
              onFocus={animateText}
              onBlur={checkEmpty}
              type="text"
              name="mobile"
              maxLength="12"
              onChange={updateInputs}
              value={
                formState.mobile.value === "" ? "" : formState.mobile.value
              }
            />
          </label>
          {formCanSubmit() === true ? (
            <button className="myhome-form-submit-cta">Submit</button>
          ) : (
            <button className="myhome-form-submit-cta disabled" disabled>
              Submit
            </button>
          )}
        </form>
        {formState.formSubmitted && (
          <p className="myhome-form-submit-noti">
            Thank you for filling up the form! The bank will contact you within
            2 business days.
          </p>
        )}
        {formState.formHasError && <p className="myhome-form-submit-noti">
            {formState.formHasError}
          </p>}
      </div>
      <span
        className={
          !formState.formIsClosed && formState.formNumChosen == bankId
            ? "myhome-callback-quit"
            : "myhome-callback-quit hide"
        }
        onClick={closeForm}
      />
    </>
  );
}

export default FormCallback;
