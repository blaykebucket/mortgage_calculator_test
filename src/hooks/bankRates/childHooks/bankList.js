import "./bankList.scss";
import React from "react";
import { useSelector, useDispatch } from "react-redux";
import { setBankInfo } from "../../../redux/actions/singleBankInfo";
import { toggleForm } from "../../../redux/actions/formCallback";

import FormCallBack from "../../formCallBack/formCallBack";

function BankList({ topBanks }) {
  const currentInputs = useSelector(state => state.calcInputs);
  const requestedSingleBank = useSelector(state => state.SingleBank);
  const callBackForm = useSelector(state => state.formCallBack);
  const dispatch = useDispatch();

  const showCallBackForm = e => {
    e.stopPropagation();
    const formNumber = e.target
      .closest(".myhome-bank-detail-group")
      .parentElement.getAttribute("data-bank-key");

    dispatch(toggleForm(formNumber));
  };

  const calculateValues = (type, val = 0) => {
    let answer = null;
    const givenInterest = parseFloat(val),
      loanAmount = currentInputs.propLoanAmt,
      tenureYears = currentInputs.propTenureYears * 12,
      rate = givenInterest / 100 / 12,
      ratePlus = 1 + rate,
      monthlyLoanNom = Math.pow(ratePlus, tenureYears) * rate,
      monthlyLoanDen = Math.pow(ratePlus, tenureYears) - 1,
      monthlyPayment = Math.round(
        (monthlyLoanNom / monthlyLoanDen) * loanAmount
      ),
      interestPay = Math.round(loanAmount * rate),
      principal = monthlyPayment - interestPay;

    switch (type) {
      case "monthly":
        answer = monthlyPayment.toLocaleString();
        break;
      case "principal":
        answer = principal;
        break;
      case "interestPay":
        answer = interestPay;
        break;
      default:
        break;
    }
    return answer;
  };

  const calculateTotalRepayment = itRates => {
    const schemePay = itRates.reduce((accum, next) => {
      const payPerYear =
        parseInt(
          calculateValues("monthly", parseFloat(next))
            .split(",")
            .filter(digit => {
              return parseInt(digit);
            })
            .join("")
        ) * 12;
      return accum + payPerYear;
    }, 0);

    ///check computation here....

    const endRate = itRates[itRates.length - 1],
      remainMonths = (currentInputs.propTenureYears - itRates.length) * 12,
      remainPay =
        parseInt(
          calculateValues("monthly", endRate)
            .split(",")
            .join("")
        ) * remainMonths;

    return (
      "$" +
      (remainMonths == 0 ? schemePay : schemePay + remainPay).toLocaleString()
    );
  };

  const requestBankListInfo = async e => {
    const bankId = e.currentTarget.getAttribute("data-bank-key");
    const uri = "http://localhost:8080/bankRates/single",
      request = new Request(uri, {
        method: "POST",
        mode: "cors",
        headers: new Headers({
          "Content-Type": "application/json"
        }),
        body: JSON.stringify({
          bankId
        })
      }),
      dataRequest = await fetch(request),
      data = await dataRequest.json();

    if (callBackForm.formNumChosen) {
      dispatch(toggleForm("0"));
    }
    dispatch(setBankInfo(data));
  };

  const extractSingleBankInfo = val => {
    const getRank = val => {
      let rank = null;
      switch (val) {
        case 0:
          rank = "1st";
          break;
        case 1:
          rank = "2nd";
          break;
        case 2:
          rank = "3rd";
          break;
        case 3:
          rank = "4th";
          break;
        case 4:
          rank = "5th";
          break;
        default:
          break;
      }
      return rank;
    };

    const { lockInPeriod, rateName, interestRates, interestRatesDetails } = val;
    return (
      <>
        <ul id="singleBankInfo">
          <li>
            <p>Rate Type:</p>
            <p>{rateName}</p>
          </li>
          <li>
            <p>Lock-in Period:</p>
            <p>{lockInPeriod}</p>
          </li>
          <li>
            <p>Total Repayment:</p>
            <p>{calculateTotalRepayment(interestRates)}</p>
          </li>
        </ul>
        <ul id="monthlyTable">
          {/* <!-- table header --> */}
          <li>
            <span>Year</span>
            <span>Interest Rate</span>
            <span>Monthly Repayment</span>
          </li>
          {/* <!-- end table header --> */}
          {interestRates.map((each, index) => {
            return (
              <li key={index}>
                <span>{getRank(index)}</span>
                <span>
                  {each + "%" + " (" + interestRatesDetails[index] + ")"}
                </span>
                <span>${calculateValues("monthly", interestRates[index])}</span>
              </li>
            );
          })}
        </ul>
        <div className="myhome-callback-cta-group">
          <div className="myhome-callback-cta-group-caption">
            <h3>Talk to an Ohmyhome Agent Today!</h3>
            <p>
              Request for a non-obligatory, free consultation with us, we will
              call you at your preferred timing
            </p>
          </div>
          <div>
            <button className="myhome-callback-cta" onClick={showCallBackForm}>
              Request Callback
            </button>
          </div>
        </div>
      </>
    );
  };

  return (
    <ul className="myhome-banklist">
      {topBanks.map(bank => {
        return (
          <li
            key={bank.bankId}
            data-bank-key={bank.bankId}
            onClick={requestBankListInfo}
          >
            <div className="myhome-bank-detail-group">
              <div className="myhome-bank-detail-group-a">
                <div className="my-home-bank-image">
                  <img src={bank.bankImage} alt={bank.bankName} />
                </div>
                <div className="my-home-bank-caption">
                  <div className="my-home-bank-caption-grp-a">
                    <h1>{bank.bankRateName}</h1>
                    <div className="my-home-interest-month">
                      <p>Interest Rate {bank.bankRate}%</p>
                      <p>
                        ${calculateValues("monthly", bank.bankRate)} / month
                      </p>
                    </div>
                  </div>
                  <div className="my-home-bank-caption-grp-b">
                    <p>
                      Principal: <br />
                      <b>${calculateValues("principal", bank.bankRate)}</b>
                    </p>
                    <span
                      className={`progress tenure-${currentInputs.propTenureYears}`}
                    />
                    <p>
                      Interest: <br />
                      <b>${calculateValues("interestPay", bank.bankRate)}</b>
                    </p>
                  </div>
                </div>
              </div>
              <div
                className={
                  requestedSingleBank.selectedBank === bank.bankId
                    ? "myhome-bank-detail-group-b show"
                    : "myhome-bank-detail-group-b"
                }
              >
                {requestedSingleBank.bankInfo &&
                  extractSingleBankInfo(requestedSingleBank.bankInfo)}
              </div>
            </div>
            <FormCallBack bankId={bank.bankId} />
          </li>
        );
      })}
    </ul>
  );
}
export default BankList;
