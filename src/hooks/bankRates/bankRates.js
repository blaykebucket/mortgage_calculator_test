import "./bankRates.scss";
import React, { useEffect } from 'react'
import { useSelector, useDispatch } from 'react-redux'
import { registerBankRates } from '../../redux/actions/bankRatesRecords'
import Spinner from '../widgets/spinner/spinner'
import BankList from "./childHooks/bankList";

function Bankrates() {

  const bankRates = useSelector((state) => state.bankRates)
  const dispatch = useDispatch();

  const fetchBanks = async () => {
    const uri = "http://localhost:8080/bankRates/all",
      request = new Request(uri, { method: "GET", mode: "cors" }),
      fetchData = await fetch(request),
      result = await fetchData.json();
    return result;
  }
  useEffect(() => {
    fetchBanks().then((res => {
      dispatch(registerBankRates(res.results))
    })).catch((err) => {
      console.log(err)
    })
  }, [])

  return (
    <section className="myhome-bankrates">
      <div className="container">
        {bankRates.topBanks.length > 0 ? <BankList topBanks={bankRates.topBanks} /> : <Spinner />}
      </div>
    </section>
  )
}

export default Bankrates
