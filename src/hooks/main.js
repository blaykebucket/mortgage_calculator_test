import React, { useEffect } from "react";
import Header from "./header/header";
import HeroShowCase from "./hero/hero";
import Bankrates from "./bankRates/bankRates";

function Main() {
  return (
    <>
      <Header />
      <HeroShowCase />
      <Bankrates />
    </>
  );
}

export default Main;
