import "./logo.scss";
import React from "react";

function Logo() {
  return (
    <span className="myhome-logo">
      <img src="./images/logo-orange.svg" alt="myhome-mobile-logo" />
      <img src="./images/logo-with-tagline.svg" alt="myhome-desktop-logo" />
    </span>
  );
}

export default Logo;
