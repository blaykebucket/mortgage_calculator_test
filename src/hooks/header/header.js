import "./header.scss";
import React from "react";
import Navigation from "../navigation/navigation";
function Header() {
  return (
    <header className="myhome-header">
      <Navigation />
    </header>
  );
}

export default Header;
