import "./app.scss"
import React from 'react'
import ReactDOM from 'react-dom'
import { Provider } from 'react-redux'
import Main from './hooks/main'
import { mainStore } from './redux/store/index'
const app = document.getElementById('app');


function App() {
  return (
    <Provider store={mainStore}>
      <Main />
    </Provider>
  )
}

ReactDOM.render(<App />, app)