# OMH Mortgage Caculator

calculator for computing property mortgage / loans.

### WebApp Screenshots

![ScreenShot](/src/screenshots/mobile-view.png)
![ScreenShot](/src/screenshots/desktop-view.PNG)

## Local Installation

```
1.Clone the Repository
2.Type npm install
3.Type npm run live
4.View Client on localhost:2020

```
