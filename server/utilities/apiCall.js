//central api caller for all routes
module.exports = async (endpoint, res, paramdata = false) => {
  const returnBody = val => {
    const { email, mobile, prpPrice, prpLoan, prpTenure } = val.body;
    return JSON.stringify({
      bankRate_id: "15",
      contactTimeSlotFlagType: 0,
      expectedLoanAmount: prpLoan,
      expectedTenure: prpTenure,
      fullName: "test data",
      mobileNumber: mobile,
      propertyPrice: prpPrice,
      mobileCountryCode: "64",
      email: email
    });
  };

  let baseURI = "https://cms.staging.ohmyhome.io/techtest/calculator";
  let baseTarget = null;

  switch (endpoint) {
    case "simplified":
      baseTarget = "/bankRates/simplified";
      break;
    case "all":
      baseTarget = "/bankRates";
      break;
    case "single":
      baseTarget = `/bankRate/info?bankRate_id=${paramdata}`;
      break;
    case "contactBank":
      baseTarget = `/form/submit`;
      break;
    default:
      console.log(`something went wrong`);
  }

  try {
    const fetchOrigin = baseURI + baseTarget,
      fetchRequest = await fetch(fetchOrigin, {
        method: endpoint === "contactBank" ? "POST" : "GET",
        headers: new Headers({
          "Content-Type": "application/json",
          "OmhWeb-Auth": process.env.OAUTH_VAL
        }),
        body: endpoint === "contactBank" ? returnBody(paramdata) : "GET"
      }),
      fetchData = await fetchRequest.json();
    res.status(200).json(fetchData);
  } catch (err) {
    res.status(400).json(err);
  }
};
