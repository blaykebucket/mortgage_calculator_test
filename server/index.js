require('dotenv').config();
require('es6-promise').polyfill();
require('isomorphic-fetch');

const express = require('express'),
  app = express(),
  port = process.env.PORT,
  path = require('path'),
  routeManager = require('./routes/router');


//consume body data
app.use(express.urlencoded({ extended: true }));
app.use(express.json())


//allow resource sharing
app.use((req, res, next) => {
  res.setHeader("Access-Control-Allow-Origin", "*");
  res.setHeader("Access-Control-Allow-Methods", "GET,POST,PUT,PATCH,DELETE");
  res.setHeader(
    "Access-Control-Allow-Headers",
    "X-Requested-With, Content-Type"
  );
  next();
})



//public files, allow only on prod
if (!process.env.NODE_ENV) {
  const publicFiles = path.resolve(__dirname, "../public");
  app.use(express.static(publicFiles));
  routeManager.serve(app, publicFiles);
} else {
  routeManager.serve(app);
}

app.listen(port, () => {
  console.log(`server running at ${port}`)
})