const apiCaller = require('../utilities/apiCall')
const bankRatesRouter = require('express').Router();


bankRatesRouter.route('/simplified')
  .get((req, res) => {
    apiCaller("simplified", res)
  })

bankRatesRouter.route('/all')
  .get((req, res) => {
    apiCaller("all", res)
  })

bankRatesRouter.route('/single')
  .post((req, res) => {
    const { bankId } = req.body;
    apiCaller("single", res, bankId)
  })


module.exports = bankRatesRouter