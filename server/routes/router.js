const bankRatesRouter = require("./bankRates");
const contactBankRouter = require("./bankContact");
exports.serve = (app, publicFiles = false) => {
  app.use("/bankrates", bankRatesRouter);
  app.use("/contactBank", contactBankRouter);
  // html file curry
  if (!process.env.NODE_ENV) {
    app.get("*", (req, res) => {
      res.status(200).sendFile(publicFiles + "/index.html");
    });
  }
};
