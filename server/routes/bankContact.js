const apiCaller = require("../utilities/apiCall");
const contactBankRouter = require("express").Router();

contactBankRouter.route("/").post((req, res) => {
  apiCaller("contactBank", res, req);
});

module.exports = contactBankRouter;
