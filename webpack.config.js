const path = require("path"),
  isDev = process.env.NODE_ENV !== "production",
  htmlPlug = require("html-webpack-plugin"),
  cssExtract = require("mini-css-extract-plugin"),
  { CleanWebpackPlugin } = require("clean-webpack-plugin"),
  copyFiles = require('copy-webpack-plugin');

module.exports = {
  mode: isDev ? "development" : "production",
  entry: path.resolve(__dirname, "src/app.js"),
  output: {
    filename: "js/app.js",
    path: path.resolve(__dirname, "public"),
    chunkFilename: "js/chunk[name].[chunkhash].js"
  },
  devServer: {
    port: 2020,
    historyApiFallback: true
  },
  module: {
    rules: [
      {
        test: /\.(js|jsx)$/,
        exclude: /node_modules/,
        use: [
          {
            loader: "babel-loader",
            options: {
              presets: ["@babel/preset-env", "@babel/preset-react"],
              plugins: ["@babel/plugin-transform-runtime"]
            }
          }
        ]
      },
      {
        test: /\.(c|sa|sc)ss$/,
        use: [
          {
            loader: cssExtract.loader,
            options: {
              publicPath: "../"
            }
          },
          {
            loader: "css-loader"
          },
          {
            loader: "sass-loader",
            options: {
              implementation: require("sass")
            }
          }
        ]
      },

    ]
  },
  plugins: [
    new CleanWebpackPlugin(),
    new cssExtract({
      filename: "css/style.css"
    }),
    new htmlPlug({
      template: path.resolve(__dirname, "src/index.html"),
      filename: "index.html"
    }),
    new copyFiles([
      {
        from: path.resolve(__dirname, "./src/images"),
        to: path.resolve(__dirname, "public/images")
      }
    ])
  ]
};
